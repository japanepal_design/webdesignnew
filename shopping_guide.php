<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">Guide to shopping on Japanese websites</a></li>
    
  </ol>
</div>


  <section class="howItWorks">      
      <div class="container">
   <div class="col-md-12">
   <div class="section-header text-center border-no">            
   <h2> Shopping on Japanese Websites

</h2>
   <p>By using your forwarding address of Japanepa.com, you can shop at Japanese websites including those that do not support international shipping. Simply list your forwarding address as your shipping address.</p>
          </div>
 <div>            
 <div class="timeline row">
<div class="timeline-icon hidden-xs"><i class="fa fa-angle-double-up"></i></div>
    <!-- timeline item start -->
    <div class="timeline-item pull-right object-non-visible">
  <img src="images/shopping.jpg">
                    </div>
                    <!-- timeline item end -->

                    <!-- timeline item start -->
 <div class="timeline-item  object-non-visible cirlce-cap">
<!-- blogpost start -->
<article class="clearfix blogpost left-hand">  
<h2>STEP 1 </h2>                     
<h2>
Choose your product</h2>  
<p>Register with us for free and receive your own Japanese address.</p>

 <p>Find your desired item and proceed to purchase it.</p>
 <a href="" class="btn btn-info ">Recommended Shopping website </a>    
<br><br>
<p>Please be aware that certain products may be prohibited in your country.</p>

<a class="btn btn-warning">Prohibited Items </a>


<a class="btn btn-warning">Conditions</a>
   

  </article>
   <!-- blogpost end -->
     </div>
     <!-- timeline item end -->
                    
                    <!-- timeline item start -->
 <div class="timeline-item object-non-visible margin-top-767" style="margin-bottom:0">
   <img src="images/address.jpg">
  </div>
                    <!-- timeline item end -->

                    <!-- timeline item start -->
   <div class="timeline-item pull-right object-non-visible cirlce-cap">
                        <!-- blogpost start -->
  <article class="clearfix blogpost right-hand">  
  <h2>STEP 2</h2>                      
<h2>Get your address</h2> 
<p>Log in at Japanepa.com and obtain your Japanepa forwarding address on "My Page".
</p>

<a href="login.php" class="btn btn-danger">Login</a>



                          
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- timeline item end -->
                    
                    <!-- timeline item start -->
<div class="timeline-item pull-right object-non-visible">
   <img src="images/address.jpg">
 </div>
 <!-- timeline item end -->

    <!-- timeline item start -->
   <div class="timeline-item object-non-visible cirlce-cap">
   <!-- blogpost start -->
   <article class="clearfix blogpost">
   <h2>STEP 3</h2>                       
<h2>Enter your forwarding address as your shipping address</h2>  
<p>Enter your Japanepa.com forwarding address (name, address, postcode etc.) as the shipping address on the website from which you would like to make a purchase. You can find your Japanepa.com forwarding address at the top of your My Page.
We have also prepared several example pages for popular shopping websites including Amazon and Rakuten to help you when entering your Japanepa.com forwarding address on Japanese websites.</p>
<div class="">
  <div class=" usage-box">
        <a class="icon-box" href="address.php" target="_self">
<div class="svg-main">
<i class="fa fa-info-circle" aria-hidden="true"></i>

</div>
<h4 class="icon-box__title">
HOW TO ENTER YOUR FORWARDING ADDRESS</h4>
<span class="icon-box__subtitle">
  Guide link to learn how to enter your tenso shipping address on online stores.   </span>
</a>
      </div>
</div>

   
</article>
   <!-- blogpost end -->



 </div>
     <!-- timeline item end -->

      <div class="timeline-item pull-right object-non-visible cirlce-cap">
                        <!-- blogpost start -->
  <article class="clearfix blogpost right-hand">  
    <h2>STEP 4 </h2>
  <h2>Item arrives at Japanepa's warehouse!</h2>                      
 
<p>Your item is now on its way to Japanepa. Once it arrives, we will register it on your My Page within one business day and send you a confirmation email. At that point, you can review the contents and shipping address before paying for international shipping and handling.
</p>

       <img src="images/onway.jpg" class="  text-center ">
                        </article>
                        <!-- blogpost end -->
                    </div>
                </div>

                    </div>
                      
        </div>
        
      </div><!-- container /- -->
    </section>
  

<?php include_once('includes/footer.php'); ?>
  </body>
</html>