
						<!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-mind-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-inverse"></i>
                    </button>
                </div>
                
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-mind-collapse">
                    <ul class="nav navbar-nav">
						<li>
										<a href="index.php" <?php if(@$menuSelected == 'home') echo 'class="active"'; ?>>Home                                                                                                                                                                                    </a>
									</li>
									<li>
										<a href="about.php" <?php if(@$menuSelected == 'about') echo 'class="active"'; ?>>About Us</a>
									</li>
									<li>
										<a href="services.php" <?php if(@$menuSelected == 'services') echo 'class="active"'; ?>>Our Services</a>
									</li>
									<li>
										<a href="choose.php" <?php if(@$menuSelected == 'choose') echo 'class="active"'; ?>>why choose us</a>
									</li>
									<li>
										<a href="blog.php" <?php if(@$menuSelected == 'blog') echo 'class="active"'; ?>>Blog</a>
									</li>
									<li>
										<a href="faq.php" <?php if(@$menuSelected == 'faq') echo 'class="active"'; ?>>Faq</a>
									</li>
									<li>
										<a href="contact.php" <?php if(@$menuSelected == 'contact') echo 'class="active"'; ?>>Contact Us</a>
									</li>
                    </ul> <!-- nav nabvar-nav -->
                </div><!-- navbar-collapse -->