<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>JapaNepa</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
<link href="css/flexslider.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link rel="stylesheet" href="css/swiper.min.css">
<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
<!-- <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> -->

<link href="https://fonts.googleapis.com/css?family=Raleway:600,900" rel="stylesheet">
 <link href="css/font-awesome.min.css" rel="stylesheet">


<link rel="stylesheet" href="css/jClocksGMT.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
   <div class="container-fluid">
<div class="row">
<img src="images/firsthead.jpg" class="img-responsive" alt="offers">
</div>
   </div>
<header id="pav-mainnav">
  <div class="container-fluid">
  <div class="row">
<div class="col-md-2 col-xs-12 col-sm-2">
<a href="index.php"  title="JapaNepa"><img src="images/newlogo.png" class="img-responsive" title="Japanepa" alt="Japanepa" ></a>
</div>

<div class="col-md-7 col-sm-5 col-xs-12"><nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    
<div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     <!--  <a class="navbar-brand" href="#"></a> -->
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="guide.php">User Guide </a></li>
        <li><a href="#">Help</a></li>
           <li><a href="#">Popular online shops</a></li>
            <li><a href="#">Proxy purchasing service</a></li>
             <li><a href="mypage.php">My page</a></li>
      </ul>
      
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>

<div class="col-md-3 col-sm-5 col-xs-12">
  <div class="rightlo">
<a href="login.php">Login</a>
<a href="register.php">Sign up</a>
<a href="" id="pop1" data-toggle="popover" data-placement="bottom"  data-html="true" data-content="<a href=> 日本語 English 繁體中文 简体中文 한글</a> " data-trigger="manual">English</a>
</div>

</div>
  </div>
  </div>
</header>
