
<!-- footer -->
<footer class="global_footer">
      
    <div class="footer_link_common_wrapper">
    <div class="main-container container">
      <div class="row">

        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="footer_link_box">
            <div class="link_box_inner">
              <h2 class="tit">User Guide</h2>
              <ul>
                <li>
                  <a href="static/guide_flow_index.html">
                    International Shipping Process                  </a>
                </li>
                <li>
                  <a href="static/guide_buy_index.html">
                    Shopping Guide                  </a>
                </li>
                <li>
                  <a href="static/guide_country_index.html">
                    Available Countries                  </a>
                </li>
                <li>
                  <a href="static/guide_deliver_index.html">
                    Shipping Methods                  </a>
                </li>
                <li>
                  <a href="static/guide_fee_index.html">
                    Usage Fees                  </a>
                </li>
                <li>
                  <a href="static/guide_payment_index.html">
                    Payment Methods                  </a>
                </li>
                <li>
                  <a href="guide/package/index.html">
                    Package Consolidation Service                  </a>
                </li>
                <li>
                  <a href="static/guide_attention_index.html">
                    Conditions                  </a>
                </li>
                <li>
                  <a href="static/guide_usage_schedule.html">
                    JapaNepa.com Business Hours                  </a>
                </li>
                <li>
                  <a href="estimate.php">
                    Shipping Fee Calculator                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div><!-- col -->

        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="footer_link_box">
            <div class="link_box_inner">
              <h2 class="tit">International Shipping Guide</h2>
              <ul>
                <li>
                  <a href="static/guide_contraband_index.html">
                    Prohibited Goods                  </a>
                </li>
                <li>
                  <a href="static/guide_contraband_lithium.html">
                    Shipping Lithium Batteries                  </a>
                </li>
              </ul>
            </div>
            <div class="link_box_inner">
              <h2 class="tit">Popular online shops</h2>
              <ul>
                <li>
                  <a href="static/shopping_index.html">
                    Japanese Online Stores                  </a>
                </li>
              </ul>
            </div><!-- link_box_inner -->
          </div><!-- footer_link_box -->
        </div><!-- col -->

        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="footer_link_box">
            <div class="link_box_inner">
              <h2 class="tit">Shopping Tips</h2>
              <ul>
                <li>
                  <a href="static/guide_advice_voltage.html">
                    Electronics (Voltage and Sockets)                  </a>
                </li>
                <li>
                  <a href="static/guide_advice_size.html">
                    Footwear and Clothing Size Charts                  </a>
                </li>
              </ul>
            </div>
            <div class="link_box_inner">
              <h2 class="tit">Help</h2>
              <ul>
                <li>
                  <a href="http://faq.JapaNepa.com/pkb_Home?l=en_US">
                    FAQ                  </a>
                </li>
                <li>
                  <a href="static/notice.html">
                    News                  </a>
                </li>
                <li>
                  <a href="inquiry/index.html">
                    Contact Us                  </a>
                </li>
              </ul>
            </div>
            <div class="link_box_inner">
              <h2 class="tit">Related Services</h2>
              <ul>
                <li>
                  <a href="http://buyee.jp/">
                    Proxy Purchasing Service &quot;Buyee&quot;                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="footer_link_box">
            <a href="estimate.php">
              <div class="link_box_inner big_box">
                <h2 class="tit"><i class="fa fa-calculator"></i>Shipping Fee Calculator</h2>
                <p class="summary">
                  EMS, AIR, SAL, Surface are available.<br>
                  Check sizes and weight limits for shipping to your country.                </p>
                <p class="summary_link">
                  Get a price                </p>
              </div>
            </a>
            <div class="link_box_inner sns_btns">
              <div class="sns_btn text-center">
                <a href="https://www.facebook.com/" title="Facebook" target="_blank">
    <span class="fa-stack fa-lg pull-left">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
</span></a>
                
              </div>
              <div class="sns_btn">
                <a href="" title="Twitter" target="_blank"><span class="fa-stack fa-lg pull-left">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
</span></a>
              </div>
                          </div>
            <div class="link_box_inner sprites-cards" style="clear: both">
              <div class="cards-icon cards-icon--min paypal"></div>
              <div class="cards-icon cards-icon--min visa"></div>
              <div class="cards-icon cards-icon--min master"></div>
              <div class="cards-icon cards-icon--min amex"></div>
              <div class="cards-icon cards-icon--min jcb"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<footer class="footer-bottom--single">
  <nav class="footer">
          <div>
        <p class="pagetop"><a href="#"></a></p>
      </div>
    
    <div class="footer_link_bottom_wrapper">
      <div class="main-container container">
        <div class="row">
        <div class="footer_logo_area col-md-2">
          <a href="index.php"><img src="images/footerlogo.png" alt="Japanepa.com"></a>
        </div>
        <div class="link_box_inner col-md-10 pull-right text-right">
          <ul>
              <li>
                <a href="static/company.html">Company Profile</a>
              </li>
              <li>
                <a href="static/terms_policy.html">Terms of Service</a>
              </li>
              <li>
                <a href="static/terms_privacy.html">Privacy Policy</a>
              </li>
              <li>
                <a href="static/terms_law.html">Act on Specified Commercial Transactions</a>
              </li>
                            <li>
                <a href="static/sitemap.html">Sitemap</a>
              </li>
          </ul>
          <div class="copyright">
            Copyright &copy; JapaNepa.com
            <span></span>
            All Rights Reserved.
          </div>
        </div>
      </div>
      </div>
    </div>
  </nav>
</footer>
  
</footer>
<p id="back-top"> <a href="#top"><span></span></a> </p>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $( document ).on( "click", "#pop1", function() {
    $( this ).popover( "toggle" );      
    return false;
});
    </script>

    <script src="js/jquery.flexslider.js"></script> 
<script type="text/javascript">
//Flexslider
jQuery('#main-slider').flexslider({
    animation: "fade",
    controlNav:"false",
  });


  
</script>



<script src="js/swiper.min.js"></script>
    
    <script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 1.7,
        centeredSlides: true,
    
         nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true,slidesPerColumn: 1,
          loop:true,
visibilityFullFit: true,

breakpoints: {
           
            768: {
                slidesPerView:1,
                spaceBetween: 0
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            }
        }

    });

    var swiper2 = new Swiper('.swiper2', {
        pagination: '.swiper-pagination',
    
          loop:true,

 slidesPerView: "1",
        spaceBetween: 0,centeredSlides: true,
          nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        pagination: '.swiper-pagination',slidesPerColumn: 1,
        paginationType: 'progress',
      

    });
    </script>
  
<script type="text/javascript">
  /*For Back to Top button*/
  $(document).ready(function(){
  $("body").append("<a class='top_button' title='Back To Top' href=''>TOP</a>");
  $(function () {
  $(window).scroll(function () {
  if ($(this).scrollTop() > 70) {
  $('.top_button').fadeIn();
  } else {
  $('.top_button').fadeOut();
  }
  });
  // scroll body to 0px on click
  $('.top_button').click(function () {
  $('body,html').animate({
  scrollTop: 0
  }, 800);
  return false;
  });
  });
  });
</script>

<script type="text/javascript">


$(document).ready(function(){

//----------------------fix nav on scroll
  

  var stickyNavOffsetTop = $('#pav-mainnav').offset().top;
  //console.log(stickyNavOffsetTop);
  var stickyNav = function(){
    var scrollTop = $(window).scrollTop();
  
  if( scrollTop > stickyNavOffsetTop ){
      $('#pav-mainnav').stop().addClass('fixNav');
    } else {
      $('#pav-mainnav').stop().removeClass('fixNav');
    }
  };  
  // run our function on load
  stickyNav();
  
  // and run it again every time you scroll
  $(window).scroll(function() {
     stickyNav();
  });
});
</script> 
<script src="js/owl.carousel.min.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    $("#testo").owlCarousel();
  
  var checkWidth = $(document).width();
  
  if(checkWidth <=600){
    $("#core-demo").owlCarousel();
  }
  
});

</script>

<script src="js/jquery.rotate.js"></script>
<script src="js/jClocksGMT.js"></script>

<script type="text/javascript">
  $('#clock_hou').jClocksGMT({ title: '',offset:'+9', skin: 4,date:true, });
</script>


<script type="text/javascript">
  $(function(){ /* to make sure the script runs after page load */

    $('a.rm').click(function(event){ /* find all a.read_more elements and bind the following code to them */

        event.preventDefault(); /* prevent the a from changing the url */
       
        $('.rm').hide(); /* hide rm button */

    });

});
</script>


<script type="text/javascript">
$(document).ready(function () {
$('.acshow').click(function (e) {
e.preventDefault();
e.stopPropagation();
$(this).next('.img-sample').slideToggle();
$(this).toggleClass('active12');
if ($(this).hasClass('active12'))
$(this).find('span').html('<i class="fa fa-chevron-down" aria-hidden="true"></i>')
else
$(this).find('span').html('<i class="fa fa-chevron-circle-right arrow_sample" aria-hidden="true"></i>')
});
$('.img-sample').click(function (e) {
e.stopPropagation();
});
$(document).click(function () {
$('.img-sample').slideUp();
});
});
</script>