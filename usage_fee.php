<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">Usage Fee</a></li>
    
  </ol>
</div>

 <section class="usage-main">
   <div class="container">
    <div class="row">
<div class="col-md-12">
      <p><h4>Our service fee of 50JPY is among the cheapest in the industry.</h4></p>

     


      <div class="well">
         <h4>Our Service Usage Fee</h4>
<p>Japanepa.com provides multi-currency payment method.</p>

<p>Based on the total cost in Japanese Yen, the relative price in each currency is determined according to the exchange rate set by the market, between 11am-noon, Japanese Standard Time.</p>
<p>The currency is determined by the payment information updated by customer, which enables you to pay in the local currency.</p>
<p><a href="">Please see this page for more details (on payment methods and payment currency).</a></p>
      </div>
      <p>Japanepa's handling fees are based on the weight of the package.</p>
<p>Our total usage fee includes the handling and international shipping fees.</p>
<div class="text-center">
<img src="images/fee.jpg" class="img-responsive  zeroauto">
</div>


</div>

<div class="col-md-12">
<div class="newprice">
  <h4 class="text-center">Nepal Fee Chart (International Shipping Fee + Handling Fee)</h4>
<table class="table table-bordered table-responsive table-striped" cellspacing="0">
  <caption></caption>
  <colgroup>
  <col class="col_tabNavi_fee" width="12.5%" style="width: 14%;">
  <col class="col_tabNavi_fee" width="12.5%" style="width: 14%;">
  <col class="col_tabNavi_fee" width="12.5%" style="width: 14%;">
  <col class="col_tabNavi_fee" width="12.5%" style="width: 14%;">
  <col class="col_tabNavi_fee" width="12.5%" style="width: 14%;">
  <col class="col_tabNavi_fee" width="12.5%" style="width: 14%;">
  <col class="col_tabNavi_fee" width="12.5%" style="width: 14%;">
  </colgroup>
  <thead>
    <tr>
      <th>Weight</th>
      <th class="main_shipping_method--ems">EMS + Handling Fee</th>
      <th>AIR/International Parcel + Handling Fee</th>
      <th>SAL/International Parcel + Handling Fee</th>
      <th>Surface Mail/International Parcel + Handling Fee</th>
      <th>AIR/Registered Small Package + Handling Fee<br>(410 yen included)</th>
      <th>SAL/Registered Small Package + Handling Fee<br>(410 yen included)</th>
    </tr>
  </thead>
  <tbody class="merged_2">
    <tr></tr><th>〜50g</th><td>1,450 yen </td><td>2,150 yen </td><td>2,250 yen </td><td>1,750 yen </td><td>580 yen </td><td>620 yen </td><tr></tr><th>〜100g</th><td>1,500 yen </td><td>2,200 yen </td><td>2,300 yen </td><td>1,800 yen </td><td>700 yen </td><td>670 yen </td><tr></tr><th>〜150g</th><td>1,550 yen </td><td>2,250 yen </td><td>2,350 yen </td><td>1,850 yen </td><td>820 yen </td><td>800 yen </td><tr></tr><th>〜200g</th><td>1,700 yen </td><td>2,400 yen </td><td>2,500 yen </td><td>2,000 yen </td><td>1,040 yen </td><td>950 yen </td><tr></tr><th>〜250g</th><td>1,750 yen </td><td>2,450 yen </td><td>2,550 yen </td><td>2,050 yen </td><td>1,160 yen </td><td>1,080 yen </td><tr></tr><th>〜300g</th><td>1,800 yen </td><td>2,500 yen </td><td>2,600 yen </td><td>2,100 yen </td><td>1,280 yen </td><td>1,130 yen </td><tr></tr><th>〜350g</th><td>1,900 yen </td><td>2,600 yen </td><td>2,700 yen </td><td>2,200 yen </td><td>1,450 yen </td><td>1,310 yen </td><tr></tr><th>〜400g</th><td>1,910 yen </td><td>2,610 yen </td><td>2,710 yen </td><td>2,210 yen </td><td>1,530 yen </td><td>1,320 yen </td><tr></tr><th>〜450g</th><td>1,920 yen </td><td>2,620 yen </td><td>2,720 yen </td><td>2,220 yen </td><td>1,610 yen </td><td>1,410 yen </td><tr></tr><th>〜500g</th><td>1,930 yen </td><td>2,630 yen </td><td>2,730 yen </td><td>2,230 yen </td><td>1,690 yen </td><td>1,420 yen </td><tr></tr><th>〜550g</th><td>2,080 yen </td><td>3,240 yen </td><td>2,740 yen </td><td>2,240 yen </td><td>1,770 yen </td><td>1,510 yen </td><tr></tr><th>〜600g</th><td>2,090 yen </td><td>3,250 yen </td><td>2,750 yen </td><td>2,250 yen </td><td>1,850 yen </td><td>1,520 yen </td><tr></tr><th>〜650g</th><td>2,240 yen </td><td>3,260 yen </td><td>2,760 yen </td><td>2,260 yen </td><td>1,930 yen </td><td>1,610 yen </td><tr></tr><th>〜700g</th><td>2,250 yen </td><td>3,270 yen </td><td>2,770 yen </td><td>2,270 yen </td><td>2,010 yen </td><td>1,620 yen </td><tr></tr><th>〜750g</th><td>2,400 yen </td><td>3,280 yen </td><td>2,780 yen </td><td>2,280 yen </td><td>2,090 yen </td><td>1,710 yen </td><tr></tr><th>〜800g</th><td>2,410 yen </td><td>3,290 yen </td><td>2,790 yen </td><td>2,290 yen </td><td>2,170 yen </td><td>1,720 yen </td><tr></tr><th>〜850g</th><td>2,560 yen </td><td>3,300 yen </td><td>2,800 yen </td><td>2,300 yen </td><td>2,250 yen </td><td>1,810 yen </td><tr></tr><th>〜900g</th><td>2,560 yen </td><td>3,300 yen </td><td>2,800 yen </td><td>2,300 yen </td><td>2,320 yen </td><td>1,810 yen </td><tr></tr><th>〜950g</th><td>2,700 yen </td><td>3,300 yen </td><td>2,800 yen </td><td>2,300 yen </td><td>2,390 yen </td><td>1,890 yen </td><tr></tr><th>〜1,000g</th><td>2,700 yen </td><td>3,300 yen </td><td>2,800 yen </td><td>2,300 yen </td><td>2,460 yen </td><td>1,890 yen </td><tr></tr><th>〜1,100g</th><td>3,100 yen </td><td>4,000 yen </td><td>3,600 yen </td><td>2,800 yen </td><td>2,735 yen </td><td>2,070 yen </td><tr></tr><th>〜1,200g</th><td>3,180 yen </td><td>4,080 yen </td><td>3,680 yen </td><td>2,880 yen </td><td>2,815 yen </td><td>2,230 yen </td><tr></tr><th>〜1,250g</th><td>3,190 yen </td><td>4,090 yen </td><td>3,690 yen </td><td>2,890 yen </td><td>2,825 yen </td><td>2,320 yen </td><tr></tr><th>〜1,300g</th><td>3,500 yen </td><td>4,100 yen </td><td>3,700 yen </td><td>2,900 yen </td><td>3,010 yen </td><td>2,330 yen </td><tr></tr><th>〜1,400g</th><td>3,600 yen </td><td>4,200 yen </td><td>3,800 yen </td><td>3,000 yen </td><td>3,110 yen </td><td>2,510 yen </td><tr></tr><th>〜1,500g</th><td>3,600 yen </td><td>4,200 yen </td><td>3,800 yen </td><td>3,000 yen </td><td>3,110 yen </td><td>2,590 yen </td><tr></tr><th>〜1,600g</th><td>3,940 yen </td><td>4,840 yen </td><td>3,840 yen </td><td>3,040 yen </td><td>3,325 yen </td><td>2,710 yen </td><tr></tr><th>〜1,700g</th><td>3,960 yen </td><td>4,860 yen </td><td>3,860 yen </td><td>3,060 yen </td><td>3,345 yen </td><td>2,810 yen </td><tr></tr><th>〜1,750g</th><td>3,970 yen </td><td>4,870 yen </td><td>3,870 yen </td><td>3,070 yen </td><td>3,355 yen </td><td>2,900 yen </td><tr></tr><th>〜1,800g</th><td>4,280 yen </td><td>4,880 yen </td><td>3,880 yen </td><td>3,080 yen </td><td>3,540 yen </td><td>2,910 yen </td><tr></tr><th>〜1,900g</th><td>4,300 yen </td><td>4,900 yen </td><td>3,900 yen </td><td>3,100 yen </td><td>3,560 yen </td><td>3,010 yen </td><tr></tr><th>〜2,000g</th><td>4,320 yen </td><td>4,920 yen </td><td>3,920 yen </td><td>3,120 yen </td><td>3,580 yen </td><td>3,110 yen </td><tr></tr><th>〜2,500g</th><td>4,950 yen </td><td>5,650 yen </td><td>4,750 yen </td><td>3,650 yen </td><tr></tr><th>〜3,000g</th><td>5,620 yen </td><td>6,420 yen </td><td>4,920 yen </td><td>3,820 yen </td><tr></tr><th>〜3,500g</th><td>6,230 yen </td><td>7,130 yen </td><td>5,730 yen </td><td>4,330 yen </td><tr></tr><th>〜4,000g</th><td>6,830 yen </td><td>7,830 yen </td><td>5,830 yen </td><td>4,430 yen </td><tr></tr><th>〜4,500g</th><td>7,430 yen </td><td>8,530 yen </td><td>6,630 yen </td><td>4,930 yen </td><tr></tr><th>〜5,000g</th><td>8,120 yen </td><td>9,320 yen </td><td>6,820 yen </td><td>5,120 yen </td><tr></tr><th>〜5,500g</th><td>8,820 yen </td><td>10,020 yen </td><td>7,620 yen </td><td>5,720 yen </td><tr></tr><th>〜6,000g</th><td>9,500 yen </td><td>10,700 yen </td><td>7,800 yen </td><td>5,900 yen </td><tr></tr><th>〜6,500g</th><td>10,470 yen </td><td>11,370 yen </td><td>8,570 yen </td><td>6,470 yen </td><tr></tr><th>〜7,000g</th><td>10,650 yen </td><td>12,050 yen </td><td>8,750 yen </td><td>6,650 yen </td><tr></tr><th>〜7,500g</th><td>11,730 yen </td><td>12,830 yen </td><td>9,630 yen </td><td>7,330 yen </td><tr></tr><th>〜8,000g</th><td>11,800 yen </td><td>13,400 yen </td><td>9,700 yen </td><td>7,400 yen </td><tr></tr><th>〜8,500g</th><td>12,780 yen </td><td>14,080 yen </td><td>10,480 yen </td><td>7,980 yen </td><tr></tr><th>〜9,000g</th><td>12,950 yen </td><td>14,750 yen </td><td>10,650 yen </td><td>8,150 yen </td><tr></tr><th>〜9,500g</th><td>13,930 yen </td><td>15,430 yen </td><td>11,430 yen </td><td>8,730 yen </td><tr></tr><th>〜10,000g</th><td>14,100 yen </td><td>16,100 yen </td><td>11,600 yen </td><td>8,900 yen </td><tr></tr><th>〜11,000g</th><td>15,250 yen </td><td>17,150 yen </td><td>12,350 yen </td><td>9,550 yen </td><tr></tr><th>〜12,000g</th><td>16,400 yen </td><td>18,200 yen </td><td>13,100 yen </td><td>10,200 yen </td><tr></tr><th>〜13,000g</th><td>17,550 yen </td><td>19,250 yen </td><td>13,850 yen </td><td>10,850 yen </td><tr></tr><th>〜14,000g</th><td>18,700 yen </td><td>20,300 yen </td><td>14,600 yen </td><td>11,500 yen </td><tr></tr><th>〜15,000g</th><td>19,500 yen </td><td>21,000 yen </td><td>15,000 yen </td><td>11,800 yen </td><tr></tr><th>〜16,000g</th><td>20,300 yen </td><td>21,700 yen </td><td>15,400 yen </td><td>12,100 yen </td><tr></tr><th>〜17,000g</th><td>21,100 yen </td><td>22,400 yen </td><td>15,800 yen </td><td>12,400 yen </td><tr></tr><th>〜18,000g</th><td>21,900 yen </td><td>23,100 yen </td><td>16,200 yen </td><td>12,700 yen </td><tr></tr><th>〜19,000g</th><td>22,700 yen </td><td>23,800 yen </td><td>16,600 yen </td><td>13,000 yen </td><tr></tr><th>〜20,000g</th><td>23,500 yen </td><td>24,500 yen </td><td>17,000 yen </td><td>13,300 yen </td><tr></tr><th>〜21,000g</th><td>24,300 yen </td><td>25,200 yen </td><td>17,400 yen </td><td>13,600 yen </td><tr></tr><th>〜22,000g</th><td>25,100 yen </td><td>25,900 yen </td><td>17,800 yen </td><td>13,900 yen </td><tr></tr><th>〜23,000g</th><td>25,900 yen </td><td>26,600 yen </td><td>18,200 yen </td><td>14,200 yen </td><tr></tr><th>〜24,000g</th><td>26,700 yen </td><td>27,300 yen </td><td>18,600 yen </td><td>14,500 yen </td><tr></tr><th>〜25,000g</th><td>27,500 yen </td><td>28,000 yen </td><td>19,000 yen </td><td>14,800 yen </td><tr></tr><th>〜26,000g</th><td>28,300 yen </td><td>28,700 yen </td><td>19,400 yen </td><td>15,100 yen </td><tr></tr><th>〜27,000g</th><td>29,100 yen </td><td>29,400 yen </td><td>19,800 yen </td><td>15,400 yen </td><tr></tr><th>〜28,000g</th><td>29,900 yen </td><td>30,100 yen </td><td>20,200 yen </td><td>15,700 yen </td><tr></tr><th>〜29,000g</th><td>30,700 yen </td><td>30,800 yen </td><td>20,600 yen </td><td>16,000 yen </td><tr></tr><th>〜30,000g</th><td>31,500 yen </td><td>31,500 yen </td><td>21,000 yen </td><td>16,300 yen </td></tbody>
  </table>
</div>
  </div>
    </div>
    
<div class="row">
<div class="col-md-12 services">
  <h4>Optional Services</h4>

  <legend>Package Consolidation Service</legend>
  <table class="table table-responsive table-bordered" cellspacing="0">
    <colgroup>
      <col width="40%">
      <col width="*">
    </colgroup>
    <tbody><tr>
      <th>Application fee</th>
      <td>200yen</td>
    </tr>
    <tr>
      <th>Service fee</th>
      <td>300yen(for every added package)</td>
    </tr>
  </tbody></table>


  <legend>Special Packaging for bottles</legend>
  <table class="table table-responsive table-bordered" cellspacing="0">
    <colgroup>
      <col width="40%">
      <col width="*">
    </colgroup>
    <tbody><tr>
      <th>Bottle-packaging styrofoam</th>
      <td>300yen(for one bottle)</td>
    </tr>
  </tbody></table>


  <p>※When your bottles arrive at our warehouse, we will notify you by email about the packaging options. If you would like the special bottle-packaging styrofoam, please let us know by replying to the notification.</p>
<p> ※Please note that some bottles may not fit in the styrofoam container due to the size restrictions.</p>
</div>
</div>

     


    
   </div>
 </section>

  
  

<?php include_once('includes/footer.php'); ?>
  </body>
</html>