<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">Usage Guide</a></li>
    
  </ol>
</div>

 <section class="usage-main">
   <div class="container">
    <div class="row">
<div class="col-md-12">
      <p>Register at Japanepa.com to get a Japanese address with which you can purchase Japanese products from overseas.
Service information for all tenso customers</p>
</div>
    </div>
    <div class="row">

      <div class="col-md-4 usage-box">
        <a class="icon-box" href="international_guide.php" target="_self">
<i class="fa  fa-calendar"></i>
<h4 class="icon-box__title">INTERNATIONAL SHIPPING</h4>
<span class="icon-box__subtitle">
  The shipment process, from registration, to package arrival at tenso, to your doorstep!                </span>
</a>
      </div>
      <div class="col-md-4 usage-box">
        <a class="icon-box" href="shopping_guide.php" target="_self">
<i class="fa  fa-cart-plus"></i>
<h4 class="icon-box__title"> SHOPPING GUIDE</h4>
<span class="icon-box__subtitle">
 
  Websites that don't support international shipping are now available to overseas customers.                             </span>
</a>
      </div>

      <div class="col-md-4 usage-box">
        <a class="icon-box" href="usage_fee.php" target="_self">
<i class="fa  fa-money"></i>
<h4 class="icon-box__title">USAGE FEE</h4>
<span class="icon-box__subtitle">
  
                Our service fee starts from just 50 yen. The total cost is just the service and shipping fees!                         </span>
</a>
      </div>
    </div>


     <div class="row">

      <div class="col-md-4 usage-box">
        <a class="icon-box" href="#" target="_self">
<i class="fa  fa-credit-card"></i>
<h4 class="icon-box__title">PAYMENT METHOD</h4>
<span class="icon-box__subtitle">
Our service fee starts from just 50 yen. The total cost is just the service and shipping fees!                          </span>
</a>
      </div>
      <div class="col-md-4 usage-box">
        <a class="icon-box" href="#" target="_self">
<i class="fa  fa-globe"></i>
<h4 class="icon-box__title"> AVAILABLE COUNTRIES</h4>
<span class="icon-box__subtitle">
   We service many countries and are constantly in the process of expanding.                                           </span>
</a>
      </div>

      <div class="col-md-4 usage-box">
        <a class="icon-box" href="#" target="_self">
<i class="fa   fa-paper-plane"></i>
<h4 class="icon-box__title">SHIPPING METHODS</h4>
<span class="icon-box__subtitle">
  We ship items internationally via EMS, AIR, SAL, or Surface!   </span>
</a>
      </div>
    </div>

<!-- third start -->
    <div class="row">

      <div class="col-md-4 usage-box">
        <a class="icon-box" href="#" target="_self">
<i class="fa  fa-clock-o"></i>
<h4 class="icon-box__title">OPERATION TIME</h4>
<span class="icon-box__subtitle">
  We inform you about the  overall operation time for our international shipping.</span>
</a>
      </div>
      <div class="col-md-4 usage-box">
        <a class="icon-box" href="#" target="_self">
<i class="fa fa-exclamation-triangle"></i>
<h4 class="icon-box__title"> CONDITIONS</h4>
<span class="icon-box__subtitle">
   Conditions of japanepa.com .Please review before using our service.                                         </span>
</a>
      </div>

      <div class="col-md-4 usage-box">
        <a class="icon-box" href="#" target="_self">
<i class="fa fa-cubes"></i>
<h4 class="icon-box__title">PACKAGE CONSOLIDATION</h4>
<span class="icon-box__subtitle">
  Our new optional service! Consolidate multiple packages into one for big savings.   </span>
</a>
      </div>
    </div>
   </div>
 </section>

  
  

<?php include_once('includes/footer.php'); ?>
  </body>
</html>