<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">Login</a></li>
    
  </ol>
</div>

  <section class="login-main">
    <div class="container">
<div class="row">
  <div class="col-md-6">

 

    <form class="form-horizontal">
<fieldset>

<!-- Form Name -->


<!-- Prepended text-->
<div class="form-group left-login">
 <legend>Login</legend>
  <div class="col-md-10">
    <div class="input-group">
      <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
      <input id="prependedtext" name="prependedtext" class="form-control" placeholder="Email Address" type="text">
    </div>
    <div class="input-group m-top">
      <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
      <input id="prependedtext" name="prependedtext" class="form-control" placeholder="Password" type="password">



    </div>
      <a href="" class="btn btn-info btn-lag">Login<i class="fa fa-sign-in" aria-hidden="true"></i></a>
    
  </div>
</div>

</fieldset>
</form>

      </div>

      <div class="col-md-6">

 

    <form class="form-horizontal">
<fieldset>

<!-- Form Name -->


<!-- Prepended text-->
<div class="form-group left-login right-login"><legend>New to JapaNepa.com?</legend>
<h2>Register for free to get a Japanese address</h2>
  <div class="">
    
    <a href="" class="btn btn-warning btn-lag">Sign up<i class="fa fa-user" aria-hidden="true"></i></a>
      
    
  </div>

  <div class="">
    <h2>Or sign up using your existing account</h2>
    
      <a href="" class="btn btn-info btn-lag">Register via Facebook<i class="fa fa-facebook" aria-hidden="true"></i></a>
    
  </div>
</div>

</fieldset>
</form>

      </div>
</div>
    </div>
  </section>


<?php include_once('includes/footer.php');
 ?>
  </body>
</html>