<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">How to Enter your tenso Address</a></li>
    
  </ol>
</div>

 

  <section class="address-main">
    <div class="container">
<div class="row">
<div class="col-md-12">
  <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs " role="tablist">
    <li role="presentation" class="active"><a href="#guide" aria-controls="home" role="tab" data-toggle="tab"><img src="images/guide.png" class="img-responsive">    </a></li>
    <li role="presentation"><a href="#rakuten" aria-controls="profile" role="tab" data-toggle="tab"><img src="images/rakuten_guide.png" class="img-responsive"></a></li>
    <li role="presentation"><a href="#amazon" aria-controls="messages" role="tab" data-toggle="tab"><img src="images/amazon_guide.png" class="img-responsive"></a></li>
    <li role="presentation"><a href="#rakuteninter" aria-controls="settings" role="tab" data-toggle="tab"><img src="images/rakutengm_guide.png" class="img-responsive"></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="guide">
      
<div class="user-address">
<h4>General Guide</h4>
<div class="sub-address">
<table class="sample_general">
      <thead>
        <tr>
          <th colspan="2" class="address_table_category"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Category</th>
          <th class="address_table_content"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;What to enter</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th class="address_th_list">A</th>
          <th class="user-th">氏名・お名前</th>
          <td>
            <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">Sunil  karanjit</span>
          </td>
        </tr>
        <tr>
          <th class="address_th_list">B</th>
          <th class="user-th">郵便番号</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;120-0023 or 1200023</td>
        </tr>
        <tr>
          <th class="address_th_list">C</th>
          <th class="user-th">都道府県</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;東京都</td>
        </tr>
        <tr>
          <th class="address_th_list">D</th>
          <th class="user-th">市区町村／郡市区（島）</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;足立区</td>
        </tr>
        <tr>
          <th class="address_th_list">E</th>
          <th class="user-th">番地／それ以降の住所</th>
          <td>
            <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;千住曙町４２－４
            <p class="address_attention"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;If the address is too long to fit in part "E", please use parts "E" and "F" to fill in the rest of the address.</p>
          </td>
        </tr>
        <tr>
          <th class="address_th_list">F</th>
          <th class="user-th">建物名・号室／マンション名／それ以降の住所</th>
          <td>
                        <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">ＴＳ７８５１５８</span>　転送コム
            <p class="address_attention"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;If full-width characters are required, please copy and paste the above address.</p>
          </td>
        </tr>
        <tr>
          <th class="address_th_list">G</th>
          <th class="user-th">電話番号</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;03-5739-3341 or 0357393341</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

    </div>
    <div role="tabpanel" class="tab-pane" id="rakuten">
      <div class="user-address">
    <h4><b>Rakuten</b></h4>
    <table>
      <thead>
        <tr>
          <th colspan="2" class="address_table_category"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Category</th>
          <th class="address_table_content"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;What to enter</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th class="address_th_list">A</th>
          <th class="user-th">氏名・姓・名</th>
          <td>
            <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">Sunil  karanjit</span>
                          <ul class="address_attention">
                <li class="align_justify fontColor-ac-d"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;If you don't know how to enter your name in kana (カナ), the native Japanese script, please use the tool below to get your name in Japanese.&nbsp;<a href="http://www.sljfaq.org/cgi/e2k.cgi"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;http://www.sljfaq.org/cgi/e2k.cgi</a></li>
                <li class="align_justify fontColor-ac-d"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;Please enter your last name in [姓], and your first name in [名]</li>
              </ul>
                      </td>
        </tr>
        <tr>
          <th class="address_th_list">B</th>
          <th class="user-th">フリガナ</th>
          <td><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">You do not have to fill in this part.</span></td>
        </tr>
        <tr>
          <th class="address_th_list">C</th>
          <th class="user-th">郵便番号</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;120-0023</td>
        </tr>
        <tr>
          <th class="address_th_list">D</th>
          <th class="user-th">都道府県</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">Please select "東京都"</span></td>
        </tr>
        <tr>
          <th class="address_th_list">E</th>
          <th class="user-th">市区郡</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;足立区</td>
        </tr>
        <tr>
          <th class="address_th_list">F</th>
          <th class="user-th">町名・番地・建物名</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;千住曙町42－4&nbsp;<span class="fontColor-ac-d">TS785158</span>&nbsp;転送コム</td>
        </tr>
        <tr>
          <th class="address_th_list">G</th>
          <th class="user-th">電話番号</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;03-5739-3341</td>
        </tr>
        <tr>
          <th class="address_th_list">H</th>
          <th class="user-th">メールアドレス</th>
          <td>
            <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">Please enter your own email address.</span>
            <p class="address_attention only_pc"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;They require you to enter it twice for confirmation</p>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="actual">
      <a href="" class="acshow"> Actual registration page <span> <i class="fa fa-chevron-circle-right arrow_sample" aria-hidden="true"></i></span></a>
      <div class="img-sample " >
<img src="images/rakuten_sample.png" class="img-responsive">
      </div>
    <div>
 
    </div>
  </div>


   </div>
    
    
  </div>
<div role="tabpanel" class="tab-pane" id="amazon">
      <div class="user-address">
    <h4><b>Amazon jp</b></h4>
    <table>
      <thead>
        <tr>
          <th colspan="2" class="address_table_category"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Category</th>
          <th class="address_table_content"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;What to enter</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th class="address_th_list">A</th>
          <th class="user-th">氏名</th>
          <td>
            <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">Sunil  karanjit</span>&nbsp;
            <p class="address_attention"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;&nbsp;English is fine.</p>
          </td>
        </tr>
        <tr>
          <th class="address_th_list">B</th>
          <th class="user-th">郵便番号</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;120-0023</td>
        </tr>
        <tr>
          <th class="address_th_list">C</th>
          <th class="user-th">都道府県</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">Please select "東京都"</span></td>
        </tr>
        <tr>
          <th class="address_th_list">D</th>
          <th class="user-th">住所1</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;足立区千住曙町４２－４</td>
        </tr>
        <tr>
          <th class="address_th_list">E</th>
          <th class="user-th">住所2</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">TS785158</span></td>
        </tr>
        <tr>
          <th class="address_th_list">F</th>
          <th class="user-th">会社名</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;転送コム</td>
        </tr>
        <tr>
          <th class="address_th_list">G</th>
          <th class="user-th">電話番号</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;03-5739-3341</td>
        </tr>
      </tbody>
    </table>

    <div class="actual">
      <a href="" class="acshow"> Actual registration page <span> <i class="fa fa-chevron-circle-right arrow_sample" aria-hidden="true"></i></span></a>
      <div class="img-sample " >
<img src="images/amazonjp_sample.png" class="img-responsive">
      </div>
    <div>
 
    </div>
  </div>


   </div>
    
    
  </div>

  <div role="tabpanel" class="tab-pane" id="rakuteninter">
      <div class="user-address">
    <h4><b>Rakuten Global Market</b></h4>
    <table>
      <thead>
        <tr>
          <th colspan="2" class="address_table_category"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Category</th>
          <th class="address_table_content"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;What to enter</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th class="address_th_list">A</th>
          <th class="user-th">First Name / Last Name</th>
          <td>
            <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">* Please enter your own name</span>&nbsp;
            <p class="address_attention"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;English is fine.</p>
          </td>
        </tr>
        <tr>
          <th class="address_th_list">B</th>
          <th class="user-th">Country</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">Please select "Japan"</span></td>
        </tr>
        <tr>
          <th class="address_th_list">C</th>
          <th class="user-th">Street Address</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">（* Please enter your 6 digits account number starting with TS.）</span> tenso.com, 42-4, Senjyuakebono-cho</td>
        </tr>
        <tr>
          <th class="address_th_list">D</th>
          <th class="user-th">Zip / Postal Code</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;120&nbsp;0023</td>
        </tr>
        <tr>
          <th class="address_th_list">E</th>
          <th class="user-th">City</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Adachi-ku</td>
        </tr>
        <tr>
          <th class="address_th_list">F</th>
          <th class="user-th">State / Province / Region</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span class="fontColor-ac-d">Please select "Tokyo"</span></td>
        </tr>
        <tr>
          <th class="address_th_list">G</th>
          <th class="user-th">Telephone Number</th>
          <td><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;03&nbsp;5739&nbsp;3341</td>
        </tr>
      </tbody>
    </table>

    <div class="actual">
      <a href="" class="acshow"> Actual registration page<span>  <i class="fa fa-chevron-circle-right arrow_sample" aria-hidden="true"></i></span></a>
      <div class="img-sample " >
<img src="images/rakutengm_sample.png" class="img-responsive">
      </div>
    <div>
 
    </div>
  </div>


   </div>
    
    
  </div>
</div>
</div>
</div>    </div>

  </section>
  

<?php include_once('includes/footer.php'); ?>
  </body>
</html>