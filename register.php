<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">Register</a></li>
    
  </ol>
</div>

  <section class="register-main">
    <div class="container">
      <div class="row">
<div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-12 register-sub">
<h2>Please enter your correct Email address</h2>
<img src="images/register.png" alt="register japanepa.com" class="text-center">

<form class="form-horizontal">
<fieldset>



<!-- Text input-->
<div class="form-group well">
  <label class=" control-label" for=""></label>  
  <div class="email-col text-center">
  <input id="" name="" type="text" placeholder="Email address" class="form-control input-md text-center">
    <a href="" class="btn btn-warning btn-lag">Sign up</a>
  </div>
</div>

</fieldset>
</form>

</div>

<div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-12 register-sub">
<p>If you have trouble receiving the signup email, you may have typed your emaill address incorrectly, or the email is getting trapped by an over-aggressive spam filter. If you have a spam filter, please make sure it will allow emails from Japanepa.com into your inbox.</p>

<p>In this case, please try to validate email address again.</p>
</div>
</div>

<div class="row">
<div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-12 register-sub">
<h2>Or sign up using your existing account</h2>


<form class="form-horizontal">
<fieldset>



<!-- Text input-->
<div class="form-group well">
  <label class=" control-label" for=""></label>  
  <div class="email-col text-center">
  
   <a href="" class="btn btn-info btn-lag">Register via Facebook<i class="fa fa-facebook" aria-hidden="true"></i></a>
  </div>
</div>

</fieldset>
</form>

</div>
  </div>
    </div>
  </section>


<?php include_once('includes/footer.php');
 ?>
  </body>
</html>