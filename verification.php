<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">Submitting Proof of Identity</a></li>
    
  </ol>
</div>
<section class="maintab">
  <div class="container">
<div class="row">
<div class="col-md-12 ">
<div class="col-md-4 pull-right rightusermenu">
  <a href="" class="username"><i class="fa fa-user"></i>Sunil karanjit - TS785158</a><a href="" class="btn btn-info logout">Logout </a>

</div>

</div>
</div>
  </div>
</section>

  <section class="howItWorks">      
      <div class="container">
   <div class="col-md-12">
   <div class="section-header text-center border-no">            
   <h2> Easy Steps of the Identity Verification Process

</h2>
   <p>In simple three step</p>
          </div>
 <div>            
 <div class="timeline row">
<div class="timeline-icon hidden-xs"><i class="fa fa-angle-double-up"></i></div>
    <!-- timeline item start -->
    <div class="timeline-item pull-right object-non-visible">
  <img src="images/howItworks/register-showcase.png">
                    </div>
                    <!-- timeline item end -->

                    <!-- timeline item start -->
 <div class="timeline-item  object-non-visible cirlce-cap">
<!-- blogpost start -->
<article class="clearfix blogpost left-hand">                       
<h2>REGISTRATION & DOCUMENTS SUBMISSION</h2>  
<p>First of all, please register the information needed to conduct the identity verification process.</p>

<p>After that, we will ask you to submit the documents to support the process. Your name and address on the documents must match those on your account. </p>     
<a href="#docume" data-toggle="collapse" class="rm">Click here to see valid documents for verifying identity</a>

<div id="docume" class="collapse">
<p><strong>Valid Documents for Verifying Identity</strong></p>
<ul class="validid">
  <li>National ID</li>
  <li>Passport</li>
<li>Driver's license</li>
<li>Credit card statement</li>
<li>Bank statement</li>
<li>Cable and Internet bill</li>
<li>Cell phone bill</li>

<li>Gas, water, electricity bill</li>
<li>Copy of family register</li>
<li>Student card</li>
<li>Rental agreement</li>
<li>Residence Card</li>
<li>Health Insurance Card</li>
</ul>    

<p  style="clear: both;"><strong>Invalid Documents for Verifying Identity</strong></p>
<ul class="invalidid">
<li>  Business Card</li>
<li>Delivery Receipts</li>
<li>Personal Letters</li>
<li>Screenshot of an order's details page</li>
</ul> 
</div>   

  </article>
   <!-- blogpost end -->
     </div>
     <!-- timeline item end -->
                    
                    <!-- timeline item start -->
 <div class="timeline-item object-non-visible margin-top-767" style="margin-bottom:0">
   <img src="images/howItworks/recieve.png">
  </div>
                    <!-- timeline item end -->

                    <!-- timeline item start -->
   <div class="timeline-item pull-right object-non-visible cirlce-cap">
                        <!-- blogpost start -->
  <article class="clearfix blogpost right-hand">                        
<h2>DOCUMENT SCREENING</h2> 
<p>The registered information and submitted documents will be screened by our staffs.</p>

<p>* It usually takes 2 business days.</p>

<p>Upon completing the document screening, we will email you regarding the results. Please refer to the email for details.</p>

<p>After the document screening is approved, you will be able to check out your packages.</p>                          
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- timeline item end -->
                    
                    <!-- timeline item start -->
<div class="timeline-item pull-right object-non-visible">
  <img src="images/howItworks/get-ready.png">
 </div>
 <!-- timeline item end -->

    <!-- timeline item start -->
   <div class="timeline-item object-non-visible cirlce-cap">
   <!-- blogpost start -->
   <article class="clearfix blogpost">                       
<h2>SHIPPING & RECEIVING THE PACKAGE</h2>  
<p>In order to confirm the current address under "The Act on Prevention of Transfer of Criminal Proceeds", we need to send your first package to your registered current address.</p>

<p>You will be able to change the shipping address the next time you use the service.</p>  

<p>Please check <a href="">here</a> if the shipping address for the first time shipping is different from your current address.</p> </article>
   <!-- blogpost end -->



 </div>
     <!-- timeline item end -->

                </div>
<a href="form.php" class=" subfix text-center btn btn-warning clearfix">Click here to submit documents</a>
                    </div>
                      
        </div>
      </div><!-- container /- -->
    </section>
  <section class="howItWorks proxy-main">      
      <div class="container">
   <div class="col-md-12">
   <div class="section-header text-center border-no">  

   <h2>Or</h2>          
   <h2> No identity verification is required to use Buyee, our proxy shopping service

</h2>
   <p>Sign up in 1 minute with your Japanepa.com account!</p>
          </div>
 <div>            
 <div class="timeline row">
<div class="timeline-icon hidden-xs"><i class="fa fa-angle-double-up"></i></div>
    <!-- timeline item start -->
    <div class="timeline-item pull-right object-non-visible">
  <img src="images/howItworks/buyee.jpg">
                    </div>
                    <!-- timeline item end -->

                    <!-- timeline item start -->
 <div class="timeline-item  object-non-visible cirlce-cap">
<!-- blogpost start -->
<article class="clearfix blogpost left-hand">                       
<h2>BUY VIA BUYEE ACCOUNT</h2>  
 
<p>If you do not possess your identification documents in hand,

we sincerely recommend Buyee, a proxy shopping service provided by tenso, inc.

Buyee will purchase products on your behalf therefore identification procedures is not required.</p>
   

  </article>
   <!-- blogpost end -->
     </div>
     
   


                </div>
<a href="http://buyee.jp/?lang=en" class=" subfix text-center btn btn-warning clearfix">Learn more about Buyee</a>
                    </div>
                      
        </div>
      </div><!-- container /- -->
    </section>

<?php include_once('includes/footer.php'); ?>
  </body>
</html>