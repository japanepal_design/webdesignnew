<div class="forhome">
<?php include_once('includes/header.php');
 ?>
</div>


 <link rel="stylesheet" type="text/css" media="screen" href="css/demo-styles.css" />
 <!--[if lt IE 8]><link rel="stylesheet" type="text/css" media="screen" href="css/demo-styles-ie.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" media="screen" href="css/sequencejs-theme.sliding-horizontal-parallax.css" />
<!-- Banner Section Start -->
  
  <div id="header">
    <div id="slideshow">
      <img class="prev" src="images/bt-prev.png" alt="Previous" />
      <img class="next" src="images/bt-next.png" alt="Next" />
      <ul>
        <li>
          <div class="info animate-in">
            <h2>Welcome </h2>
            <p>3 Simple Steps from registration, to package arrival at Nepajapa.com, to your doorstep.</p>   <a href="international_guide.php" class="btn btn-danger">LEARN MORE</a>
          </div>
          <img class="sky animate-in" src="images/bg-clouds.png" alt="Blue Sky" />
          <img class="balloon animate-in" src="images/balloon.png" alt="Balloon" />
        </li>
        <li>
          <div class="info">
            <h2>SHIPPING</h2>
            <p>Upon receiving your payment we will ship the product that you purchased from online sites.</p>
            <a href="international_guide.php" class="btn btn-danger">LEARN MORE</a>
          </div>
          <img class="sky " src="images/bg-clouds.png" alt="Blue Sky" />
          <img class="aeroplane" src="images/aeroplane.png" alt="Aeroplane" />
        </li>
        <li>
          <div class="info">
            <h2>package delivery</h2>
            <p>Thank you for using Japanepa.com .Your package will be delivered at your doorstep.</p>
          </div>
          <img class="sky animate-in" src="images/bg-clouds.png" alt="Blue Sky" />
          <img class="balloon" src="images/kite.png" alt="Kite" />
        </li>
      </ul>
    </div>
  </div>

<section class="howtoti">
  <div class="container">
<div class="col-md-12">
  <h2 class="text-center">How to use JapaNepa.com</h2>

  <p class="text-center titletxt">Just one-time registration,<br>
and you can get Japanese items in 3 simple steps.</p>
</div>
  </div>
</section>




<section class="steps  services-negative-top">
  <div class="container">
<div class="row">
<div class="col-md-4 col-sm-4">
<div class="service-feature-box">
<div class="service-media">
<img src="images/box1.jpg" alt="Trucking">

<a href="overland-transportation.html" class="read-more02">
<span>Recommended Shops<i class="fa fa-chevron-right"></i></span></a>
   </div><!-- .service-media end -->
 <div class="service-body">
 <div class="custom-heading">
  <h4>Step 1.Shopping</h4>
 </div><!-- .custom-heading end -->
<p>
   You can shop at your favorite online stores. You don't even have to be familiar with Japanese sites!
   </p>
 </div><!-- .service-body end -->
 </div><!-- .service-feature-box-end -->
                    </div>

<div class="col-md-4 col-sm-4">
<div class="service-feature-box">
<div class="service-media">
<img src="images/box2.jpg" alt="Trucking">

<a href="overland-transportation.html" class="read-more02">
<span>Our Service Flow<i class="fa fa-chevron-right"></i></span></a>
   </div><!-- .service-media end -->
 <div class="service-body">
 <div class="custom-heading">
  <h4>Step 2.Domestic Shipping</h4>
 </div><!-- .custom-heading end -->
<p>
   Your ordered product will be shipped to  japanepa's warehouse and  we will then ship out the package from our warehouse to you.
   </p>
 </div><!-- .service-body end -->
                        </div><!-- .service-feature-box-end -->
                    </div>

                    <div class="col-md-4 col-sm-4">
<div class="service-feature-box">
<div class="service-media">
<img src="images/box3.jpg" alt="Trucking">

<a href="overland-transportation.html" class="read-more02">
<span style="font-size: 12px;padding-left: 7px;">Available Shipping Methods<i class="fa fa-chevron-right"></i></span></a>
   </div><!-- .service-media end -->
 <div class="service-body">
 <div class="custom-heading">
  <h4>Step3.International Shipping</h4>
 </div><!-- .custom-heading end -->
<p>
   Your ordered product will be shipped to your home via your desired shipping method such as EMS, AIR, SAL, or Surface Mail.
   </p>
 </div><!-- .service-body end -->
                        </div><!-- .service-feature-box-end -->
                    </div>
</div>
  </div>
  
</section>


<section id="featured-section"  >
<div class="container">
<div class="row">
  <div class="col-md-12">
<h2 class="text-center">  Customer stories</h2>
  </div>
  <div class="col-md-12">
    <div id="myCarousel-re" class="carousel slide carousel-fade" data-ride="carousel" >
      <ol class="carousel-indicators">
        <li data-target="#myCarousel-re" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel-re" data-slide-to="1"></li>
        <li data-target="#myCarousel-re" data-slide-to="2"></li>
      </ol>
      
      <div class="carousel-inner">
        <div class="item active">
          <div class="ftd-item">
            <figure>
              <img src="images/featured-item.jpg">
            </figure>
            <figcaption>
            <h2>Mr Bikash Prajapati</h2>
            <h3>The Handy & Reliable</h3>
            <p>The tablet I bought from amazon.co.jp through JapaNepa.com was handy & reliable</p>
            
            </figcaption>
          </div>
        </div>
        <div class="item ">
          <div class="ftd-item">
            <figure>
              <img src="images/featured-item3.jpg">
            </figure>
            <figcaption>
            <h2>Mr Sunil Karanjit</h2>
            <h3>Good value for money</h3>
            <p>It was difficult for me to buy the online product in competitie price .Luckily I found JapaNepa.com </p>
           
            </figcaption>
          </div>
        </div>
        <div class="item ">
          <div class="ftd-item">
            <figure>
              <img src="images/featured-item1.jpg">
            </figure>
            <figcaption>
            <h2>Mr Anil Maharjan</h2>
            <h3>Fast delivery</h3>
            <p>I ordered a camera from Rakuten online store and it was delivered within a week in my home.  </p>
            <p>I am very happy with the servive of JapaNepa.com</p>
            </figcaption>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>


<!-- core value -->
<section class="core_value">
  <div class="container">  <div class="row">
<div class="col-md-12 text-center">

<h2>JapaNepa.com's core values</h2>
</div>
</div>


<div class="row">
<div class="banner-boxs" id="core-demo">
  
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
  <div class="banner-box-col">
<figure>
<img src="images/box-img/1.jpg"/>
</figure>
<div class="banner-box-txt">
<div class="box-txt">
<h3> Customer Satisfaction</h3>
<p><span>The JapaNepa.com service is built upon customers' requests and voices.</span></p>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-box-col">
<figure>
<img src="images/box-img/2.jpg"/>
</figure>
<div class="banner-box-txt">
<div class="box-txt">
<h3>Safe delivery</h3>
<p><span> We pack your items in the most suitable way for secure delivery. </span></p>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-box-col">
<figure>
<img src="images/box-img/3.jpg"/>
</figure>
<div class="banner-box-txt">
<div class="box-txt">
<h3>Reliable customer support service</h3>
<p><span> We will answer your questions with sincerity.</span></p>
</div>
</div>
</div>
</div>
            </div>
          </div>
  </div>
</section>


<!-- <section id="results"  >
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <div class="wrap">
      <h1 class="tit">Long-term success<br class="hide__on_sm_up"> thanks to loyal customers</h1><br><br>
      <p>The constant support of our customers <br>has enabled us to become the longest-running Japanese forwarding company in the business.</p>
    </div>
  </div>
</div>
</div>
  </section> -->
<section class="youcancount">
  
  <div class="container">
<div class="row">

  <div class="col-md-12">
    <h2>You can count on JapaNepa.com!</h2>

    <ul>
      <li><img src="images/japan.jpg" alt="America" class="img-responsive img-rounded"><span>Japan</span></li>
      <li><img src="images/ship.jpg" alt="America" class="img-responsive img-rounded"><span>forwarding service</span></li>

      <li><img src="images/nepal.jpg" alt="America" class="img-responsive img-rounded"><span>Nepal</span></li>
    </ul>


    <h2>NepaJapa.com offers optimum forwarding service,
from Japan to  Nepal.
</h2>
  </div>
</div>  </div>
</section>



  <!-- <section class="signup">
    
<div class="container">
<div class="row">
  <div class="col-md-12">
    <h1>  Let's get started!</h1>
    <h2>Countless Japanese goods await you here!</h2>

    <a href="" class="btn btn-success btn-lg">Sign Up</a>
  </div>
</div>
  </div>
  </section>
 -->
   <section class="we-are-best">
      <div class="auto-container container">
          <div class="row clearfix">
              
<div class="col-md-6 col-sm-6 col-xs-12 image-side">
 <figure class="image"><img class="img-responsive" src="images/signup.jpg" alt="" title=""></figure>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 text-side">
<h2> Let's get started!</h2>
<h3>Countless Japanese goods await you here!</h3>
             <a href="register.php" class="btn btn-danger btn-lg">Sign Up</a>    
                </div>
                
            </div>
        </div>
    </section>

  <!-- <section id="home_link_wrapper">
   <div class="container">
   <div class="row">
    <div class="">
    <div class="main-container">

      <div class="col-md-3">
      <div class="footer_link_box">
       
        <i class="fa fa-info"></i>
        <h2 class="tit">NepaJapa.com</h2>
        <div class="summary"><p>Nepajapa.com offers international forwarding service from Japanese online shops to the world.</p></div>
        <div class="link">
          <a href="">
            More about us          </a>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="footer_link_box">
        <i class="fa fa-exclamation-triangle"></i>
        <h2 class="tit">Cautions</h2>
        <div class="summary"><p>There are items which NepaJapa.com cannot handle.</p><p>Please check our Conditions of Use carefully before purchasing.</p></div>
        <div class="link">
          <a href="static/guide_attention_index.html">
            Conditions of use          </a>
        </div>
      </div>
    </div>
      
<div class="col-md-3">
      <div class="footer_link_box">
        <i class="fa fa-question-circle"></i>
        <h2 class="tit">Help</h2>
        <div class="summary"><p>Please visit our FAQ for questions and inquiries.</p><p></p></div>
        <div class="link">
          <a href="">
            FAQ          </a>
        </div>
      </div>

    </div>

    <div class="col-md-3">
      <div class="footer_link_box">
                  <i class="fa fa-calculator"></i>
          <h2 class="tit">Fee Calculator</h2>
          <div class="summary"><p>Get a quick estimate of JapaNepa.com usage fees here. Also check sizes and weight limits for shipping to your country.</p></div>
          <div class="link">
            <a href="">
              Get a price            </a>
          </div>
              </div>
            </div>
    </div>
  </div>
  </div>
  </div>
  </section> -->


<?php include_once('includes/footer.php');
 ?>
 <script type="text/javascript" src="js/sequence.jquery-min.js"></script>
  <script type="text/javascript"> 
    $(document).ready(function(){
      var options = {
        swipeNavigation:false,
        autoPlay: true,
        autoPlayDirection: 1,
        nextButton: ".next",
        prevButton: ".prev",
        prependNextButton: false,
        prependPrevButton: false,
        pauseIcon: false,
        animateStartingFrameIn: false, 
        delayDuringOutInTransitions: false
      };

      var slideShow = $("#slideshow").sequence(options).data("sequence");
      
      $(".prev, .next").show();
    })
  </script>
  </body>
</html>