<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">Registration for the Identity Verification</a></li>
    
  </ol>
</div>

 

  <section class="registration-main">
    <div class="container">
    <div class="col-md-12">
    <form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Main Information</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label text-left" for="Full Name">Full Name<span class="required">Required</span></label>  
  <div class="col-md-4">
  <input id="Full Name" name="Full Name" type="text" placeholder="Enter Your full name" class="form-control input-md">
  <span class="help-block">E.g: John Smith</span>  
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label text-left" for="">Name in alphabet<span class="required">Required</span></label>  
  <div class="col-md-4">
  <input id="" name="" type="text" placeholder="Enter your name in alphabet" class="form-control input-md">
  <span class="help-block">E.g: John Smith</span>  
  </div>
</div>




<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Sex</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="radios-0">
      <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
      Male
    </label> 
    <label class="radio-inline" for="radios-1">
      <input type="radio" name="radios" id="radios-1" value="2">
      Female
    </label>
  </div>
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Occupation</label>
  <div class="col-md-4">
    <select id="selectbasic" name="selectbasic" class="form-control">
      <option value="1">Company Employee</option>
      <option value="2">Company Official</option>
       <option value="3">Self Employed</option>
        <option value="4">Civil Servant</option>
         <option value="5">Housekeeper</option>
          <option value="6">Student</option>
           <option value="8">Unemployed</option>

           <option value="9">Other</option>
    </select>
  </div>
</div>


<legend class="mtop">Current Address (Shipping Address)</legend>

<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Country/Region <span class="required">Required</span></label>
  <div class="col-md-4">
    <select id="selectbasic" name="selectbasic" class="form-control">
      
     
    <option value="0" class="gray">
    Select Country</option>
    <option value="1009" class="black">
    Argentina</option>
    <option value="1012" class="black">
    Australia</option>
    <option value="1013" class="black">
    Austria</option>
    <option value="1014" class="black">
    Azerbaijan</option>
    <option value="1017" class="black">
    Bangladesh</option>
    <option value="1018" class="black">
    Barbados</option>
    <option value="1019" class="black">
    Belarus</option>
    <option value="1020" class="black">
    Belgium</option>
    <option value="1024" class="black">
    Bhutan</option>
    <option value="1029" class="black">
    Brazil</option>
    <option value="1031" class="black">
    Brunei</option>
    <option value="1032" class="black">
    Bulgaria</option>
    <option value="1035" class="black">
    Cambodia</option>
    <option value="1037" class="black">
    Canada</option>
    <option value="1042" class="black">
    Chile</option>
    <option value="1043" class="black">
    China</option>
    <option value="1051" class="black">
    Costa Rica</option>
    <option value="1053" class="black">
    Croatia</option>
    <option value="1056" class="black">
    Czech Republic</option>
    <option value="1057" class="black">
    Denmark</option>
    <option value="1063" class="black">
    Egypt</option>
    <option value="1064" class="black">
    El Salvador</option>
    <option value="1069" class="black">
    Estonia</option>
    <option value="1073" class="black">
    Fiji</option>
    <option value="1074" class="black">
    Finland</option>
    <option value="1075" class="black">
    France</option>
    <option value="1082" class="black">
    Germany</option>
    <option value="1086" class="black">
    Greece</option>
    <option value="1090" class="black">
    Guam</option>
    <option value="1098" class="black">
    Hong Kong</option>
    <option value="1099" class="black">
    Hungary</option>
    <option value="1100" class="black">
    Iceland</option>
    <option value="1101" class="black">
    India</option>
    <option value="1105" class="black">
    Ireland</option>
    <option value="1106" class="black">
    Israel</option>
    <option value="1107" class="black">
    Italy</option>
    <option value="1109" class="black">
    Japan</option>
    <option value="1115" class="black">
    Korea (South)</option>
    <option value="1118" class="black">
    Lao People's Democratic Republic</option>
    <option value="1119" class="black">
    Latvia</option>
    <option value="1124" class="black">
    Liechtenstein</option>
    <option value="1125" class="black">
    Lithuania</option>
    <option value="1126" class="black">
    Luxembourg</option>
    <option value="1127" class="black">
    Macau</option>
    <option value="1128" class="black">
    Macedonia</option>
    <option value="1131" class="black">
    Malaysia</option>
    <option value="1132" class="black">
    Maldives</option>
    <option value="1140" class="black">
    Mexico</option>
    <option value="1143" class="black">
    Monaco</option>
    <option value="1144" class="black">
    Mongolia</option>
    <option value="1146" class="black">
    Morocco</option>
    <option value="1151" selected="" class="black">
    Nepal</option>
    <option value="1152" class="black">
    Netherlands</option>
    <option value="1154" class="black">
    New Caledonia</option>
    <option value="1155" class="black">
    New Zealand</option>
    <option value="1163" class="black">
    Norway</option>
    <option value="1165" class="black">
    Pakistan</option>
    <option value="1170" class="black">
    Peru</option>
    <option value="1171" class="black">
    Philippines</option>
    <option value="1173" class="black">
    Poland</option>
    <option value="1174" class="black">
    Portugal</option>
    <option value="1175" class="black">
    Puerto Rico</option>
    <option value="1176" class="black">
    Qatar</option>
    <option value="1178" class="black">
    Romania</option>
    <option value="1179" class="black">
    Russia</option>
    <option value="1248" class="black">
    Saipan</option>
    <option value="1188" class="black">
    Saudi Arabia</option>
    <option value="1194" class="black">
    Singapore</option>
    <option value="1195" class="black">
    Slovakia</option>
    <option value="1196" class="black">
    Slovenia</option>
    <option value="1199" class="black">
    South Africa</option>
    <option value="1068" class="black">
    Spain</option>
    <option value="1203" class="black">
    Sri Lanka</option>
    <option value="1211" class="black">
    Sweden</option>
    <option value="1212" class="black">
    Switzerland</option>
    <option value="1213" class="black">
    Taiwan</option>
    <option value="1216" class="black">
    Thailand</option>
    <option value="1222" class="black">
    Tunisia</option>
    <option value="1223" class="black">
    Turkey</option>
    <option value="1228" class="black">
    Ukraine</option>
    <option value="1229" class="black">
    United Arab Emirates</option>
    <option value="1085" class="black">
    United Kingdom</option>
    <option value="1231" class="black">
    United States</option>
    <option value="1239" class="black">
    Vietnam</option>

    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label text-left" for="">Phone Number<span class="required">Required</span></label>  
  <div class="col-md-4">
  <input id="" name="" type="text" placeholder="Enter Your Phone number" class="form-control input-md">
  <span class="help-block">E.g: +977-9801234567</span>  
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label text-left" for="">FAX Number </label>  
  <div class="col-md-4">
  <input id="" name="" type="text" placeholder="Enter your FAX number" class="form-control input-md">
  <span class="help-block">E.g: 977 1 4410159</span>  
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label text-left" for="">Zip / Postal Code<span class="required">Required</span></label>  
  <div class="col-md-4">
  <input id="" name="" type="text" placeholder="Enter your zip postal code" class="form-control input-md">
  <span class="help-block">
* If you do not have a Zip Code, please enter a hyphen (-).</span>  
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label text-left" for="">Address Line (in Alphabet)<span class="required">Required</span></label>  
  <div class="col-md-4">
  <input id="" name="" type="text" placeholder="Enter your address" class="form-control input-md">
  <span class="help-block">
E.g: Lalitpur</span> 

<input id="" name="" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">
E.g: 4-7-35</span>  

<input id="" name="" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">
  
E.g: Godawari ward number 11 </span>  
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label text-left" for="Full Name">Company</label>  
  <div class="col-md-4">
  <input id="Full Name" name="Full Name" type="text" placeholder="Enter your company name" class="form-control input-md">
  <span class="help-block">
E.g: tenso, inc.</span>  
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label text-left" for="Full Name">City <span class="required">Required</span></label>  
  <div class="col-md-4">
  <input id="Full Name" name="Full Name" type="text" placeholder="Enter your city" class="form-control input-md">
  <span class="help-block">

E.g: Sinagawa-ku</span>  
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label text-left" for="Full Name">State / Province / Region <span class="required">Required</span></label>  
  <div class="col-md-4">
  <input id="Full Name" name="Full Name" type="text" placeholder="Enter your State / Province / Region" class="form-control input-md">
  <span class="help-block">

  
E.g: Tokyo
</span>  
  </div>
</div>

<div class="text-center">
    <label for="checkbox_a1">
 <input type="checkbox" name="identity_checkbox" id="checkbox_a1" value="1">
 <span >By clicking the "Submit documents", you have checked that the registered current address is correct.</span>
              </label>
         </div>
</fieldset>

<a href="" class=" formbtn text-center btn btn-warning clearfix">Click here to submit documents</a>

<p class="text-center">* You can also register your information first, and submit the verification documents later.</p><br>
</form>

    </div>
  </div>
  </section>
  

<?php include_once('includes/footer.php'); ?>
  </body>
</html>