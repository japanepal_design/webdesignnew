<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">My Page</a></li>
    
  </ol>
</div>
<section class="maintab">
  <div class="container">
<div class="row">
<div class="col-md-12 ">
<div class="col-md-4 pull-right rightusermenu">
  <a href="" class="username"><i class="fa fa-user"></i>Sunil karanjit - TS785158</a><a href="" class="btn btn-info logout">Logout </a>

</div>

</div>
</div>
  </div>
</section>
  <section class="mypage-main">
<div class="container">
<div class="row">
<div class="col-md-9">

  <div class="user-address">
    <h3><p>Your forwarding address</p></h3>

    <div class="my_address-details">
   <dl><dt> <p class="notic"><strong>Please use the following address and phone number when you shop from Japanese online stores.</strong></p></dt></dl>

    <dl>
      <dt><p><i class="fa fa-envelope"></i> Shipping address for online shopping</p></dt>
      <dd>
        〒120-0023<br>
        <p lang="ja">
          東京都　足立区　千住曙町４２－４<!-- important: do not remove this comment
          --><span class="hide__onsmallscreen">　</span><!-- important: do not remove this comment
          --><br class="hide__onnormalscreen"><!-- important: do not remove this comment
          -->ＴＳ７８８６２５　転送コム
        </p>
      </dd>
    </dl>
    <dl>
      <dt><p><i class="fa fa-user"></i> Name</p></dt>
      <dd><p>Sunil Karanjit</p></dd>
    </dl>
    <dl>
      <dt><p><i class="fa fa-phone"></i> Telephone number for online shopping</p></dt>
      <dd>03-5739-3341</dd>
    </dl>
  </div>

  <div class="linkTo__right">
    <p><a href="address.php" target="_blank">
      How to enter your tenso address</a>
    </p>
  </div>
  </div>

<div class="user-address">
    <h3><p>Status of Identity Verification Process</p></h3>

    <p class="lead">
    According to Japanese law, package forwarding companies must perform an identity and address verification from 1st April, 2013.  </p>

    <ul class="timeline" id="timeline">
  <li class="li complete">
    <div class="timestamp">
      <span class="author">Step 1</span>
     </div>
    <div class="status">
      <h4> SUBMIT DOCUMENTS

 </h4>
    </div>
  </li>
  <li class="li ">
    <div class="timestamp">
      <span class="author">Step 2</span>
      </div>
    <div class="status">
      <h4> PROGRESSING</h4>
    </div>
  </li>
  <li class="li ">
    <div class="timestamp">
      <span class="author">Step 3</span>
     </div>
    <div class="status">
      <h4 > VERIFICATION COMPLETED </h4>
    </div>
  </li>
  
 </ul>     

  <h5><i class="fa fa-file-text-o"></i> Please submit documents for proof of identity.</h5>

  <a href="verification.php" class="btn btn-info pull-right">Click here to submit documents</a>

  <div class="infog">
    <p>If you cannot provide or it is difficult for you to provide valid documents for identity verification, please <a href="" target="_blank"><strong>click here</strong></a>.</p>
  </div>
  </div>

  <div class="user-address newsa">
    <h3><p>News</p></h3>
    <a href=""><span class="date-news">2017/10/05</span>  Resumption of package delivery to Mexico</a>


    <a href=""><span class="date-news">2017/03/17</span>  Changes to international shipping packages with item value over 200,000 yen</a>

    <a href=""><span class="date-news">2017/03/09</span>  [Nintendo Switch] In regard to International Shipping of Nintendo Switch</a>

    <a href="" class="pull-right">See full list of news</a>
  </div>

  <div class="user-address newsa">
    <h3><p>Amazon deals</p></h3>
    <a href="">20% OFF coupon - sportswear & shoes SALE!</a>
    <a href="">
Up to 10% OFF - Halloween Special!</a>

<a href="">Updated weekly! - Amazon drugstore coupons, MAX50%OFF!</a>

<a href="">Amazon time sales - Lightning deals & Limited-time sales, new surprises everyday!</a>
  </div>


<div class="user-address newsa">

    <h3><p>Package and Payment Information</p></h3>

      <p>Process your Payment from here.</p>

      <div class="panel panel-info">
    <div class="panel-body">
      <div class="col-md-2">
      <span><i class="fa fa-plane fa-4x" aria-hidden="true"></i></span>
   </div>
   <div class="col-md-10">
    <h6>International Shipments (<span class="red">0</span>)</h6>
    <a href="">International shipping fee, consolidation service, shipping history and tracking numbers.</a>
   </div>
    </div>
  </div>


  <div class="panel panel-info">
    <div class="panel-body">
      <div class="col-md-2">
      <span><i class="fa fa-plus fa-4x" aria-hidden="true"></i></span>
   </div>
   <div class="col-md-10">
    <h6>Optional services (<span class="red">0</span>)</h6>
    <a href="">International shipping fee, consolidation service, shipping history and tracking numbers.</a>
   </div>
    </div>
  </div>
</div>

<div class="user-address newsa ">
    <h3><p>Account Information</p></h3>
    <div class="review">
    <h4> <span><i class="fa fa-home   " aria-hidden="true"></i></span> Review and edit shipping address</h4>
    <a href="">You cannot change your shipping address until your package arrives at your verified address.</a>


    <h4> 
      <span><i class="fa fa-money   " aria-hidden="true"></i></span>Review and edit payment information</h4>
    <a href="">Edit your payment information here.</a>


     <h4>  <span><i class="fa fa-user   " aria-hidden="true"></i></span>Review and edit other user information</h4>
    <a href="">Edit your email address and password from here.</a>
  </div>
  </div>
    
  </div>
<div class="col-md-3">

  <div class="user-address text-center">
    <p>Before using tenso.com, make sure to review the conditions of using our service.</p>
    <a href="" class="btn btn-danger">Conditions</a>
  </div>


  <div class="user-address text-center">
    <p>Contact japanepa.com's Customer Support Team.</p>
    <a href="" class="btn btn-warning">Conditions</a>
  </div>

  <div class="user-address text-center">
   <div class="fb-page" data-href="https://www.facebook.com/JapaNepacom-1508477922551316/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/JapaNepacom-1508477922551316/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/JapaNepacom-1508477922551316/">JapaNepa,com</a></blockquote></div>
  </div>


  <div class="user-address text-center">
   <p>Track your EMS packages</p>
   <a href="" class="btn btn-info">Ems Tracking</a>
  </div>

<div class="user-address text-center">
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">Current Japan local time (JST)</h4>
  </div>
  <div class="panel-body">
      
<div id="clock_hou"></div>
  </div>
</div>
</div>

</div>
</div>

</div>

   </section>


<?php include_once('includes/footer.php');
 ?>
  </body>
</html>