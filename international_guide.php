<?php include_once('includes/header.php');
 ?>

 <div class="container">
    <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">International Shipment Process</a></li>
    
  </ol>
</div>


  <section class="howItWorks">      
      <div class="container">
   <div class="col-md-12">
   <div class="section-header text-center border-no">            
   <h2> 3 Simple Steps from registration, to package arrival at Nepajapa.com, to your doorstep.

</h2>
   <p>In simple three step</p>
          </div>
 <div>            
 <div class="timeline row">
<div class="timeline-icon hidden-xs"><i class="fa fa-angle-double-up"></i></div>
    <!-- timeline item start -->
    <div class="timeline-item pull-right object-non-visible">
  <img src="images/step1.png">
                    </div>
                    <!-- timeline item end -->

                    <!-- timeline item start -->
 <div class="timeline-item  object-non-visible cirlce-cap">
<!-- blogpost start -->
<article class="clearfix blogpost left-hand">  
<h2>STEP 1 </h2>                     
<h2>Register at Japanepa.com and get your " forwarding address"</h2>  
<p>Register with us for free and receive your own Japanese address.</p>

     


   

  </article>
   <!-- blogpost end -->
     </div>
     <!-- timeline item end -->
                    
                    <!-- timeline item start -->
 <div class="timeline-item object-non-visible margin-top-767" style="margin-bottom:0">
   <img src="images/step2.png">
  </div>
                    <!-- timeline item end -->

                    <!-- timeline item start -->
   <div class="timeline-item pull-right object-non-visible cirlce-cap">
                        <!-- blogpost start -->
  <article class="clearfix blogpost right-hand">  
  <h2>STEP 2</h2>                      
<h2>Shop at Japanese websites</h2> 
<p>Overseas customers can now shop at Japanese websites that don’t support international shipping. Simply enter your tenso address as your shipping address.
</p>

<a href="">More Details</a>

<div class="">
  <div class=" usage-box">
        <a class="icon-box" href="international_guide.php" target="_self">
<div class="svg-main">
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<path style="fill:#FEA832;" d="M279.2,32.302c-20.7-14.7-48.3-14.399-68.699,0.3c-11.7,8.401-22.301,19.2-29.5,31.201
  c-9.3-15.601-22.5-28.801-39-37.8c-18.001-9.901-39.6-9-56.7,2.699c-17.401,12.001-26.4,32.1-23.699,53.101
  c4.199,27.599,28.2,48.6,56.999,50.7c0.601,0.3,1.5,0.3,2.401,0.3h121c0.901,0,1.5,0,2.401-0.3c25.8-1.8,47.999-18.6,54.899-41.7
  C305.9,68.002,298.4,45.802,279.2,32.302z M123.701,102.803c-16.201,0-30.3-11.1-32.401-25.201c-1.201-9.6,2.699-18.6,10.8-24
  c7.8-5.4,17.401-6,25.201-1.5c19.499,10.8,32.999,29.399,37.2,50.7L123.701,102.803L123.701,102.803z M270.5,82.103
  c-3.3,11.999-16.5,20.7-31.201,20.7h-41.8c3.9-18.3,15.101-34.801,30.7-45.901c11.7-8.399,25.201-6,33.6-0.3
  C266,59.603,274.999,67.702,270.5,82.103z"/>
<path style="fill:#FE9923;" d="M279.2,32.302c-20.7-14.7-48.3-14.399-68.699,0.3c-11.7,8.401-22.301,19.2-29.5,31.201v69h61
  c0.901,0,1.5,0,2.401-0.3c25.8-1.8,47.999-18.6,54.899-41.7C305.9,68.002,298.4,45.802,279.2,32.302z M270.5,82.103
  c-3.3,11.999-16.5,20.7-31.201,20.7h-41.8c3.9-18.3,15.101-34.801,30.7-45.901c11.7-8.399,25.201-6,33.6-0.3
  C266,59.603,274.999,67.702,270.5,82.103z"/>
<rect x="30" y="162.8" style="fill:#FD3018;" width="302" height="240"/>
<rect x="181" y="162.8" style="fill:#E61E14;" width="151" height="240"/>
<path style="fill:#17CEB4;" d="M332,372.803v60H15c-8.401,0-15-6.601-15-15v-30c0-8.401,6.599-15,15-15H332z"/>
<rect x="181" y="372.8" style="fill:#11AD94;" width="151" height="60"/>
<path style="fill:#787780;" d="M181,432.803c0,16.5-6.599,31.5-17.701,42.299c-10.8,11.1-25.8,17.701-42.299,17.701
  c-32.999,0-60-27.001-60-60c0-16.5,6.599-31.5,17.701-42.301c10.8-11.1,25.8-17.699,42.299-17.699s31.5,6.599,42.299,17.699
  C174.401,401.304,181,416.304,181,432.803z"/>
<path style="fill:#57555C;" d="M181,432.803c0,16.5-6.599,31.5-17.701,42.299c-10.8,11.1-25.8,17.701-42.299,17.701v-120
  c16.5,0,31.5,6.599,42.299,17.699C174.401,401.304,181,416.304,181,432.803z"/>
<path style="fill:#17CEB4;" d="M362,192.803h-30v240h135c24.853,0,45-20.147,45-45v-45L362,192.803z"/>
<path style="fill:#57555C;" d="M512,307.702v35.101H407c-6.899,0-13.5-1.5-19.2-4.501c-9.3-4.2-17.1-11.999-21.299-21.299
  c-3.001-5.7-4.501-12.301-4.501-19.2v-105h61.8c32.1,0,60.901,20.7,71.1,51.299l13.2,39.6C510.801,291.503,512,299.603,512,307.702z
  "/>
<path style="fill:#3C3A41;" d="M431.901,294.203L387.8,338.302c-9.3-4.2-17.1-11.999-21.299-21.299l44.099-44.101
  c6-6,15.3-6,21.301,0S437.901,288.203,431.901,294.203z"/>
<path style="fill:#FD4E26;" d="M347,102.803c8.401,0,15,6.599,15,15v60c0,8.399-6.599,15-15,15H15c-8.401,0-15-6.601-15-15v-60
  c0-8.401,6.599-15,15-15C15,102.803,347,102.803,347,102.803z"/>
<path style="fill:#FD3018;" d="M347,102.803c8.401,0,15,6.599,15,15v60c0,8.399-6.599,15-15,15H181v-90
  C181,102.803,347,102.803,347,102.803z"/>
<rect x="121" y="102.8" style="fill:#FEDB41;" width="121" height="270"/>
<rect x="181" y="102.8" style="fill:#FCBF29;" width="61" height="270"/>
<path style="fill:#787780;" d="M452,432.803c0,16.5-6.599,31.5-17.701,42.299c-10.8,11.1-25.8,17.701-42.299,17.701
  c-32.999,0-60-27.001-60-60c0-16.5,6.599-31.5,17.701-42.301c10.8-11.1,25.8-17.699,42.299-17.699s31.5,6.599,42.299,17.699
  C445.401,401.304,452,416.304,452,432.803z"/>
<path style="fill:#57555C;" d="M452,432.803c0,16.5-6.599,31.5-17.701,42.299c-10.8,11.1-25.8,17.701-42.299,17.701v-120
  c16.5,0,31.5,6.599,42.299,17.699C445.401,401.304,452,416.304,452,432.803z"/>
<path style="fill:#CAE8F9;" d="M422,432.803c0,16.5-13.5,30-30,30s-30-13.5-30-30s13.5-30,30-30S422,416.304,422,432.803z"/>
<path style="fill:#B7E0F6;" d="M422,432.803c0,16.5-13.5,30-30,30v-60C408.5,402.803,422,416.304,422,432.803z"/>
<path style="fill:#CAE8F9;" d="M151,432.803c0,16.5-13.5,30-30,30s-30-13.5-30-30s13.5-30,30-30S151,416.304,151,432.803z"/>
<path style="fill:#B7E0F6;" d="M151,432.803c0,16.5-13.5,30-30,30v-60C137.5,402.803,151,416.304,151,432.803z"/>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>

</div>
<h4 class="icon-box__title">ARRIVAL</h4>
<span class="icon-box__subtitle">
  Your ordered package will arrive at tenso.com warehouse.              </span>
</a>
      </div>
</div>

<div class="">
  <div class=" usage-box">
        <a class="icon-box" href="international_guide.php" target="_self">
<div class="svg-main">
<img  src="images/open.png">

</div>
<h4 class="icon-box__title">PACKAGE REGISTRATION</h4>
<span class="icon-box__subtitle">
  After a package arrives at our warehouse, we open, check and weigh it, then register the package into your account and you will be notified via email.         </span>
</a>
      </div>
</div>                          
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- timeline item end -->
                    
                    <!-- timeline item start -->
<div class="timeline-item pull-right object-non-visible">
  <img src="images/step3.png">
 </div>
 <!-- timeline item end -->

    <!-- timeline item start -->
   <div class="timeline-item object-non-visible cirlce-cap">
   <!-- blogpost start -->
   <article class="clearfix blogpost">
   <h2>STEP 3</h2>                       
<h2>Package confirmation and payment</h2>  
<p>Please confirm your package information and submit your payment.</p>
<div class="">
  <div class=" usage-box">
        <a class="icon-box" href="international_guide.php" target="_self">
<div class="svg-main">
<img src="images/inter.png">

</div>
<h4 class="icon-box__title">
INTERNATIONAL SHIPMENT</h4>
<span class="icon-box__subtitle">
 Upon receiving your payment, Japanep.com will ship your package.         </span>
</a>
      </div>
</div>

<div class="">
  <div class=" usage-box">
        <a class="icon-box" href="international_guide.php" target="_self">
<div class="svg-main">
<img  src="images/clear.png">

</div>
<h4 class="icon-box__title">CLEARANCE FROM CUSTOMS</h4>
<span class="icon-box__subtitle">
  Customs must confirm that the content of your package is not prohibited in your country. Customs procedures and laws depend on the destination country and the content of the package </span>
</a>
      </div>
</div>   
</article>
   <!-- blogpost end -->



 </div>
     <!-- timeline item end -->

      <div class="timeline-item pull-right object-non-visible cirlce-cap">
                        <!-- blogpost start -->
  <article class="clearfix blogpost right-hand">  
  <h2>Finally your package will be delivered</h2>                      
 
<p>Thank you for using Japanepa.com .Your package will be delivered at your doorstep.
</p>

       <img src="images/thankyou.jpg" class="  text-center ">
                        </article>
                        <!-- blogpost end -->
                    </div>
                </div>

                    </div>
                      
        </div>
        
      </div><!-- container /- -->
    </section>
  

<?php include_once('includes/footer.php'); ?>
  </body>
</html>